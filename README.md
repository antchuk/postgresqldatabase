# C# Developer. Professional courses

This project presents one of the homework assignments as part of the course C# Developer.

## Table of contents

0. Scrum, GIT
1. Databases: relational databases and working with them [repository](https://gitlab.com/antchuk/postgresqldatabase.git)
2. REST и RESTful API [repository](https://gitlab.com/antchuk/resthomework.git)
3. SOLID principles [repository](https://gitlab.com/antchuk/otus_solid_homework.git)
4. Reflection [repository](https://gitlab.com/antchuk/otus_homework_serialization.git)
5. Working with methods as variables (delegates, events) [repository](https://gitlab.com/antchuk/otus_delegate_events_homework.git)
6. Introduction to concurrency in .NET. Differences between process, thread, domain and task [repository](https://gitlab.com/antchuk/otus_homework_parallel_2.git)
7. In-Process Communication [repository](https://gitlab.com/antchuk/otus_multi_thread.git)
8. Generative Design Patterns [repository](https://gitlab.com/antchuk/otus_homework_10.git)
9. Graduation work: Schedule management. Microservice based web application. [repository](https://gitlab.com/otus-c-sharp-2023-team-2/team2-diplom.git)

## Databases: relational databases and working with them

Purpose:
In this homework you will learn how to create a database with tables, as well as write scripts for populating tables with data.

Steps:
..*Create a PostgreSQL database for one of the companies to choose from: Avito, SberBank, Otus or eBay. Write a script for creating 3 tables that must have primary keys and be joined by foreign keys.
..*Write a script for filling tables with data, at least five lines each.
..*Create a console program that displays the contents of all tables.
..*Add to the program the ability to add to a table of your choice.