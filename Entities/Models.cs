﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSQLdatabase.Models
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionType> TransactionsType { get; set; }
        public ApplicationContext()
        {
            try
            {
                Database.EnsureCreated();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"ERROR in ApplicationContext creation {ex}");
            }
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=postgres;Username=user;Password=user");
        }
    }
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }            
        public int Age { get; set;}
        public string Adress { get; set; }           
    }
    /// <summary>
    /// 
    /// </summary>
    public class Transaction
    {
        public int Id { get; set; }
        public float summ { get; set; }
        public User User { get; set; }
        public TransactionType TransactionType { get; set; }
        }
    /// <summary>
    /// 
    /// </summary>
    public class TransactionType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    
}
