﻿using Microsoft.EntityFrameworkCore;
using PostgreSQLdatabase.Controllres;
using PostgreSQLdatabase.Models;


namespace PostgreSQLdatabase
{
    internal class Program
    {        
        static void Main(string[] args)
        {
            //welcome message
            Console.WriteLine("Start app\nAll data:");
            //print Users
            InfoController _controller = new InfoController();
            Console.WriteLine(_controller.GetTableAsText("Users"));
            //print Trabnsactions
            Console.WriteLine(_controller.GetTableAsText("Transactions"));
            //print TransactionsType 
            Console.WriteLine(_controller.GetTableAsText("TransactionsType"));
            //input table name
            Console.WriteLine("Print target table name (Users/Transactions/TransactionsType) or 'Exit' to finish the app");
            string _TableName = Console.ReadLine();
            while (_TableName != "Exit")
            {
                //input fields
                InputController _contrl = InputController.GetInputController();
                switch (_TableName)
                {
                    case "Users":
                        _contrl.InputUser();
                        break;
                    case "Transactions":
                        _contrl.InputTransaction();
                        break;
                    case "TransactionsType":
                        _contrl.InputTransactionType();
                        break;
                    default:
                        Console.WriteLine("Wrong table name! Please try again.");
                        break;
                }
                Console.WriteLine("Print target table name or 'Exit' to finish the app");
                _TableName= Console.ReadLine();
            };
        }
    }
}