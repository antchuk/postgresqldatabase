﻿using PostgreSQLdatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSQLdatabase.Controllres
{
    /// <summary>
    /// Получение всех записей таблицы БД как текста
    /// </summary>
    public class InfoController
    {
        public InfoController() 
        {
        }
        private string GetUsers()
        {
            string ans="";
            using (ApplicationContext db = new ApplicationContext())
            {
                var users = db.Users.ToList();
                ans = $"{ans}\nID/FirstName/LastName/Age/Adress";
                foreach (User u in users)
                {
                    ans = $"{ans}\n{u.Id}/{u.FirstName}/{u.LastName}/{u.Age}/{u.Adress}";
                }
            }
            return ans;
        }
        private string GetTransactions()
        {
            string ans = "";
            using (ApplicationContext db = new ApplicationContext())
            {
                var trans = db.Transactions.ToList();
                ans = $"{ans}\nID/summ/ID_User/ID_TransactionType";
                foreach (Transaction tr in trans)
                {
                    ans = $"{ans}\n{tr.Id} {tr.summ} {tr.User} {tr.TransactionType}";
                }
            }
            return ans;
        }
        private string GetTransactionsType()
        {
            string ans = "";
            using (ApplicationContext db = new ApplicationContext())
            {
                ans = $"{ans}\nID/Name";
                var trans = db.TransactionsType.ToList();
                foreach (TransactionType tr in trans)
                {
                    ans = $"{ans}\n{tr.Id} {tr.Name}";
                }
            }
            return ans;
        }
        /// <summary>
        /// получить все хаписи таблицы как текст
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GetTableAsText(string name)
        {
            switch (name)
            {
                case "Users":
                    return GetUsers();
                case "Transactions":
                    return GetTransactions();
                case "TransactionsType":
                    return GetTransactionsType();
                default:
                    return "something gone wrong";
            }
        }
    }
}
