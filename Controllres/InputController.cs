﻿using PostgreSQLdatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSQLdatabase.Controllres
{
    class InputController
    {
        /// <summary>
        /// ввод записей в базу данных
        /// </summary>
        private InputController() { }
        private static InputController _inpcontrol;
        private static readonly object _lock = new object();
        public static InputController GetInputController() 
        {
            if (_inpcontrol == null)
            {
                lock (_lock) 
                {
                    if (_inpcontrol == null)
                    {
                        _inpcontrol = new InputController();                        
                    }
                }
            }
            return _inpcontrol;
        }
        /// <summary>
        /// Ввод нового пользователя !без проверки, что такой уже существует!
        /// </summary>
        /// <returns></returns>
        public bool InputUser()
        {
            using (ApplicationContext _context = new ApplicationContext())
            {
                User NewUser = new User();
                Console.WriteLine("Input First Name:");
                NewUser.FirstName = Console.ReadLine();
                Console.WriteLine("Input Last Name:");
                NewUser.LastName = Console.ReadLine();
                Console.WriteLine("Input Age:");
                if (int.TryParse(Console.ReadLine(), out int age))
                {
                    NewUser.Age = age;
                }
                else
                {
                    return false;
                }
                Console.WriteLine("Input Adress:");
                NewUser.Adress = Console.ReadLine();
                Console.WriteLine("Input email:");
                NewUser.Email = Console.ReadLine();
                _context.Users.Add(NewUser);
                _context.SaveChanges();
            }
            return true;
        }
        /// <summary>
        /// ввод новой транзакции
        /// </summary>
        /// <returns></returns>
        public bool InputTransaction()
        {
            using (ApplicationContext _context = new ApplicationContext())
            {
                Transaction NewTransaction = new Transaction();
                Console.WriteLine("Input Sum:");
                if (float.TryParse(Console.ReadLine().Replace('.',','), out float _summ))
                {
                    NewTransaction.summ = _summ;
                }
                else
                {
                    NewTransaction.summ = 0;
                }
                Console.WriteLine("Input transaction type (Debit/Credit/Refund/ChargeBack/Authorization):");
                string TypeName = Console.ReadLine();
                if (TypeName != null &&
                    (TypeName == "Debit" || 
                    TypeName == "Credit" || 
                    TypeName == "Refund" || 
                    TypeName == "ChargeBack" ||
                    TypeName == "Authorization"))
                {
                    var _trType = _context.TransactionsType.Where(p=> p.Name == TypeName);
                    foreach (TransactionType _t in _trType)
                        NewTransaction.TransactionType = _t;
                }
                else
                {
                    return false;
                }
                Console.WriteLine("Input UserID:");
                if (int.TryParse(Console.ReadLine(), out int _userid))
                {
                    var _user = _context.Users.Find(_userid);
                    if (_user != null)
                    {
                        NewTransaction.User = _user;
                    }
                    else return false;
                }
                else
                {
                    return false;
                }
                _context.Transactions.Add(NewTransaction);
                _context.SaveChanges();
            }
            return true;
        }
        /// <summary>
        /// ввод нового типа транзакции !без проверки, что такой уже существует!
        /// </summary>
        /// <returns></returns>
        public bool InputTransactionType()
        {
            using (ApplicationContext _context = new ApplicationContext())
            {
                TransactionType NewTrType = new TransactionType();
                Console.WriteLine("Input First Name:");
                NewTrType.Name = Console.ReadLine();
                if (NewTrType.Name != null && NewTrType.Name != "")
                {
                    _context.TransactionsType.Add(NewTrType);
                    _context.SaveChanges();
                }
                else return false;
            }
            return true;
        }
    }
}
